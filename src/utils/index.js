// TODO: workout how to display 1.5 as 1.50!
// Would be better using third party library for things like this

export const GBPFormatter = new Intl.NumberFormat('en-GB', {
  style: 'currency',
  currency: 'GBP',
  minimumFractionDigits: 0,
  maximumFractionDigits: 2
});

