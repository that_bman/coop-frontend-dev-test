import { data as coop } from './coop.json';

const MOCK_API_ENDPOINT = 'https://coop-mock-test-api.herokuapp.com/';

module.exports = {
  get: jest.fn(url => {
    switch (url) {
      case MOCK_API_ENDPOINT:
      return Promise.resolve({
        data: coop,
      });
      default:
        return false;
    }
  }),
};
