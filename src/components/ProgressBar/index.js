import React from 'react';
import PropTypes from 'prop-types';

import './ProgressBar.css';

const ProgressBar = ({ percentage, message }) => {
  return (
    <div className="progressBar">
      <p className="progressBar__message">{message}</p>
      <div
        className="progressBar__bar"
        style={{ width: `${percentage > 100 ? 100 : percentage}%` }}
      />
    </div>
  );
};

ProgressBar.propTypes = {
  percentage: PropTypes.number.isRequired,
};

export default ProgressBar;
