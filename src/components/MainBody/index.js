import React from 'react';

import './MainBody.css';

const MainBody = ({ children }) => {
  return <div className="mainBody">{children}</div>;
};



export default MainBody;
