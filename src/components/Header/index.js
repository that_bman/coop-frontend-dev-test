import React from 'react';

import logo from '../../assets/static/images/logos/coop-logo.svg';
import './Header.css';

const Header = () => {
  return (
    <header className="header">
      <img src={logo} className="header__logo" alt="logo" />
      <h1 className="header__title">Co-op frontend dev test by Bharat Darji</h1>
    </header>
  );
};

export default Header;
