import React, { Component } from 'react';

import Header from '../Header';
import MainBody from '../MainBody';
import CharityDonation from '../CharityDonation';

import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header />
        <MainBody>
          <CharityDonation />
        </MainBody>
      </div>
    );
  }
}

export default App;
