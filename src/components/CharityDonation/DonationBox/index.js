import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './DonationBox.css';

class CharityDonation extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.submitHandler = this.submitHandler.bind(this);
    this.state = {
      inputField: '',
    };
  }

  submitHandler(evt) {
    evt.preventDefault();

    this.props.onDonate(this.state.inputField);

    this.setState({
      inputField: '',
    });
  }

  handleChange(event) {
    this.setState({
      inputField: event.target.value,
    });
  }

  render() {
    return (
      <div className="donationBox">
        <p>{this.props.message}</p>
        <div className="donationBox__form">
          <div className="donationBox__form-input">
            <span className="inputFieldCurrencySymbol">£</span>
            <input
              id="donationAmount"
              type="text"
              ref={element => {
                this.textInput = element;
              }}
              value={this.state.inputField}
              onChange={this.handleChange}
            />
          </div>
          <div className="donationBox__form-button">
            <button className="btn" onClick={this.submitHandler}>
              Donate
            </button>
          </div>
        </div>
      </div>
    );
  }
}

CharityDonation.propTypes = {
  message: PropTypes.string,
  onDonate: PropTypes.func.isRequired
};

CharityDonation.defaultProps = {
  message: 'Donate to this project',
};

export default CharityDonation;
