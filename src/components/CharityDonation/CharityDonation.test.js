import React from 'react';
import { shallow } from 'enzyme';
import CharityDonation from '.';

const flushPromises = () => new Promise(resolve => setImmediate(resolve));

it('snapshot test', async () => {
  const wrapper = shallow(<CharityDonation />);
  await flushPromises();
  wrapper.update();
  expect(wrapper).toMatchSnapshot();
});


