import React from 'react';
import PropTypes from 'prop-types';

import ProgressBar from '../../ProgressBar';

import { GBPFormatter } from '../../../utils';

import './TotalRaised.css';

const TotalRaised = ({ raisedSoFar, target }) => {
  const percentage = Math.floor((raisedSoFar / target) * 100);
  return (
    <div className="totalRaised">
      <ProgressBar
        percentage={percentage}
        message={percentage >= 100 ? 'Target Reached!!! Thank you' : `${percentage}%`}
      />
      <div className="totalRaised__form">
        <div className="totalRaised__raisedSoFar">
          <p>Raised so far </p>
          <p className="currency">{GBPFormatter.format(raisedSoFar)}</p>
        </div>
        <div className="totalRaised__target">
          <p>Target</p>
          <p className="currency">{GBPFormatter.format(target)}</p>
        </div>
      </div>
    </div>
  );
};

TotalRaised.propTypes = {
  raisedSoFar: PropTypes.number,
  target: PropTypes.number,
};

TotalRaised.defaultProps = {
  raisedSoFar: 0,
  target: 0,
};

export default TotalRaised;
