import React from 'react';
import { shallow, mount } from 'enzyme';
import TotalRaised from '.';

it('renders without crashing', () => {
  shallow(<TotalRaised />);
});

it('renders the raised so far number', () => {
  const wrapper = mount(<TotalRaised raisedSoFar={999.55} />);

  const d = wrapper.find('.totalRaised__raisedSoFar');

  expect(d.text()).toContain('999.55');
});


it('renders the target number', () => {
  const wrapper = mount(<TotalRaised target={4500} />);

  const d = wrapper.find('.totalRaised__target');

  expect(d.text()).toContain('4,500');
});

