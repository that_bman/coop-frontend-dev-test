import React, { Component } from 'react';
import axios from 'axios';

import TotalRaised from './TotalRaised';
import DonationBox from './DonationBox';

import './CharityDonation.css';

const MOCK_API_ENDPOINT = 'https://coop-mock-test-api.herokuapp.com/';

class CharityDonation extends Component {
  constructor() {
    super();
    this.state = {
      raisedSoFar: 0,
      target: 0,
      message: 'Donate to this project',
    };
    this.addDonation = this.addDonation.bind(this);
  }

  componentDidMount() {
    axios.get(MOCK_API_ENDPOINT).then(res => {
      const apiData = res.data;
      this.setState({
        raisedSoFar: apiData.raised,
        target: apiData.target,
      });
    });
  }

  addDonation(data) {
    const donation = parseInt(data, 10);

    if (!isNaN(donation) && donation > 0) {
      this.setState(prevState => ({
        raisedSoFar: prevState.raisedSoFar + donation,
        message: 'Thank you very much!',
      }));
    } else {
      this.setState({
        message: 'Looks like a bad number!',
      });
    }
  }

  render() {
    return (
      <div className="charityDonation">
        <div className="charityDonation__body">
          <h2 className="charityDonation__title">
            Help refugees rebuild their lives and communities in Manchester
          </h2>
          <p>Manchester Refugee Support Network (MRSN)</p>

          <TotalRaised raisedSoFar={this.state.raisedSoFar} target={this.state.target} />
          <DonationBox onDonate={this.addDonation} message={this.state.message} />
        </div>
        <div className="charityDonation__footer">
          <a href="http://www.google.co.uk">Learn more about causes local to you</a>
        </div>
      </div>
    );
  }
}

export default CharityDonation;
