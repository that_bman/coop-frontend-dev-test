# COOP Frontend Dev Test
## Bharat Darji / 07593 637664 / bharat@b-design.co.uk

**Date Completed: 02 September 2018**

### Main Scripts:

- `yarn install` to install node packages :)
- `yarn start` to run in dev mode
- `yarn test` to run the test files
- `yarn build` to build a production version

### Caveats and what I'd do if I had more time
- I didn't spend too much time getting it looking *exactly* as the design
- I didn't really cross browser test it - only Chrome on Mac High Sierra
- I've added *some* responsive design, but it's not full
- I loaded in the whole of the coop-frontend-toolkit sass files, my plan was to understand and use it but due to time constraints I only ended up using the minimal and added my own on top per component
- The project needs more tests - again due to time constraints, it's at a bare minimum
- There's no e2e tests

